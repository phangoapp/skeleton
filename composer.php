<?php

$arr_composer=array(

    "name" => "phangoapp/skeleton",
    "description" => "A framework for create nice apps",
    "php" => "^7.0|| ^7.3",
    "license" => "GPL",
    "authors"=> [
        array(
            "name"=> "Antonio de la Rosa",
            "email"=> "antonio.delarosa@salirdelhoyo.com"
        )
    ],
    
    "minimum-stability" => "stable",

    "repositories" => [
       array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/pharouter2.git"
        ),/*,
	array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/phamodels.git"
        ),*/
        array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/phautils.git"
        ),/*
	array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/phai18n.git"
        ),
	array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/phalibs.git"
        ),
         array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/lang.git"
        ),
	array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/phatime.git"
        ),
        array(
            "type"=> "vcs",
            "url"=> "https://bitbucket.org/phangoapp/showmedia.git"
        )*/
	],

    "require" => array(

    "phangoapp/pharouter2"=> "dev-master",
	/*"phangoapp/phamodels"=> "dev-master",*/
	"phangoapp/phautils"=> "dev-master",
/*	"phangoapp/phai18n"=> "dev-master",
	"phangoapp/phalibs"=> "dev-master",
	"phangoapp/lang"=> "dev-master",
	"phangoapp/phatime"=>"dev-master",
	"phangoapp/showmedia"=>"dev-master",*/
	"ext-gd"=> "*",
	"ext-libxml"=> "*",
	"league/climate"=> "@stable",
    "swiftmailer/swiftmailer"=> "^6.0", 
    "twig/twig" => "^2.0"	
    )

);

?>
