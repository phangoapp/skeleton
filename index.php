<?php

use PhangoApp\PhaRouter2\Router;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaI18n\I18n;

include(__DIR__."/vendor/autoload.php");


$route=new Router();

//$route->arr_finish_callbacks=array('PhangoApp\PhaModels\Webmodel::save_cache_query' => []);
/*
Utils::load_config('config_i18n');*/
Utils::load_config('config_routes');
Utils::load_config('config_apps');

if(!defined('COOKIE_SESSION_NAME'))
{

    define('COOKIE_SESSION_NAME', 'phango');

}
/*
I18n::load_lang('common');
*/
/**Load configurations from modules**/

foreach(Router::$apps as $admin_module)
{
    
    Utils::load_config('config', $path='vendor/'.$admin_module."/settings");
    
}

Utils::load_config('config');

date_default_timezone_set(PhangoApp\PhaTime\DateTime::$timezone);

$path_info=isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';

echo $route->response($path_info);

?>
